import unittest
import scrapemonsterjobs
from common.Job import Job

class TestScrapeMonsterJobs(unittest.TestCase):
    def test_calculateNumberOfPagesRequiredFor1Job(self):
        self.assertEqual(scrapemonsterjobs.calculateNumberOfPagesRequired(1), 1)
    
    def test_calculateNumberOfPagesRequiredFor256Jobs(self):
        self.assertEqual(scrapemonsterjobs.calculateNumberOfPagesRequired(256), 10)

    def test_getFullUrlFor26Jobs(self):
        query = "myquery"
        location = "mylocation"
        expectedUrl = 'https://www.monster.com/jobs/search/?q={}&where={}'.format(query, location)
        numJobs = 26
        url = scrapemonsterjobs.getFullUrl(query, location, numJobs)
        self.assertEqual(url, expectedUrl)
    
    def test_getFullUrlFor27Jobs(self):
        query = "myquery"
        location = "mylocation"
        expectedUrl = 'https://www.monster.com/jobs/search/?q={}&where={}&stpage=1&page=2'.format(query, location)
        numJobs = 27
        url = scrapemonsterjobs.getFullUrl(query, location, numJobs)
        self.assertEqual(url, expectedUrl)
    
    def test_getBeautifulSoupOnGoogle(self):
        bs = scrapemonsterjobs.getBeautifulSoup("https://www.google.com/")
        self.assertFalse(bs is None)

if __name__ == "__main__":
    unittest.main()