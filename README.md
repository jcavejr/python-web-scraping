# Web Scraping
## Overview
Contained in this repository is code used to learn web scraping in Python.

Python 3.6.9 was used with the requests and Beautifulsoup4 libraries.

## Usage
To run a search with the default settings, run:
```bash
./scrapemonsterjobs
```

For additional usage, run:
```bash
./scrapemonsterjobs -h
```

## Installation notes
You must have python 3.6 or later installed along with pip.

Once Python and pip are installed, to install required dependencies:

```bash
pip install --user -r requirements.txt
```

## Installation notes for developers
For development it is recommended to use a virtual environment. Do the following to do so:

```bash
pip install --user virtualenv
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Running unit tests
Unit tests can be run with:
```bash
python -m unittest discover
```

## Credits
This project started by following Martin Breuss' tutorial at https://realpython.com/beautiful-soup-web-scraper-python/. It was then extended into something slightly more complex.