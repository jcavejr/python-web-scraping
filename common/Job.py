import json

class Job:
    def __init__(self, title="", link="", company="", location=""):
        self._title = title
        self._link = link
        self._company = company
        self._location = location
    
    def toDict(self):
        return self.__dict__
    
    def getTitle(self):
        return self._title
    
    def setTitle(self, title):
        self._title = title
    
    def getCompany(self):
        return self._company
    
    def setCompany(self, company):
        self._company = company
    
    def getLocation(self):
        return self._location
    
    def setLocation(self, location):
        self._location = location
    
    def __str__(self):
        astr = "Title:\t\t" + self._title + \
            "\nLink:\t\t" + self._link + \
            "\nCompany:\t" + self._company + \
            "\nLocation:\t" + self._location
        return astr